var webpack = require("webpack"),
  path = require('path'),
  libPath = path.join(__dirname, 'lib'),
  wwwPath = path.join(__dirname, 'www'),
  pkg = require('./package.json'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "development",
  entry: path.join(libPath, 'index.js'),
  output: {
    path: wwwPath,
    filename: 'prototype-[hash:6].js'
  },
  module: {
    rules: [{
      test: /\.html$/,
      use: {
        loader: 'html-loader'
      }
    }, {
      test: /\.(png|jpg)$/,
      use: {
        loader: 'file-loader?name=img/[name].[ext]'
      }
    }, {
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: ["ng-annotate-loader", "babel-loader?presets[]=es2015"]
    }, {
      test: /\.(scss|css)$/,
      use: [
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS, using Node Sass by default
      ]
    }, {
      test: [/ionicons\.svg/, /ionicons\.eot/, /ionicons\.ttf/, /ionicons\.woff/],
      loader: 'file-loader?name=fonts/[name].[ext]'
    }]
  },
  resolve: {
      extensions: ['.js', '.json', '.scss', '.html'],
      modules: [
          libPath,
          path.join(__dirname, 'node_modules')
      ]
  },
  plugins: [
      new HtmlWebpackPlugin({
          filename: 'index.html',
          pkg: pkg,
          template: path.join(libPath, 'index.ejs')
      })
  ]
}