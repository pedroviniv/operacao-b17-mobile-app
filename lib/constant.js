import firebaseConfig from '../config/firebase.json';

let constant = angular.module('app.constant', [])
  .constant('firebaseConfig', firebaseConfig);

export default constant = constant.name
