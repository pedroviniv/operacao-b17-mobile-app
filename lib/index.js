import 'ionic-sdk/release/js/ionic.bundle';

// Our modules
//import ionicConfig from './config/ionic.config';
import constants from './constant';
import run from './run';
import auth from './states/auth/auth.module';
import home from './states/home/home.module';
import feed from './states/feed/feed.module';
import newPost from './states/new-post/new-post.module';

// Style entry point
import './scss/bootstrap';

const requires = [
    'ionic',
    'ui.router',
    'firebase',
    constants,
    auth,
    feed,
    home,
    newPost
];

// Create our prototype module
let app = angular.module('app', requires);
// IONIC CONFIG
//app.config(ionicConfig);
// Run
app.run(run);

export default app = app.name;
