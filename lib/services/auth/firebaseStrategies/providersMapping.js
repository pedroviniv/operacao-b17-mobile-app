const map = {
  "twitter": () => (new firebase.auth.TwitterAuthProvider()),
  "facebook": () => (new firebase.auth.FacebookAuthProvider())
};

export default map;