import authService from './auth.service';

const requires = [
  'firebase'
];

let authServiceModule = angular.module('app.shared.authService', requires);
authServiceModule.service('authService', authService);

export default authServiceModule = authServiceModule.name;