import providersMapper from './firebaseStrategies/providersMapping';

export default function ($log) {
  'ngInject';

  /**
   * Creates a promise that will open a popup authentication based
   * on the provider passed by parameter.
   * Once the promise is finished and the permission is granted, an user instance
   * can be catched.
   * 
   * If some error occurs, the error info can be catched by the promise.
   * 
   * accepted providers:
   *   facebook
   *   twitter
   */
  this.signIn = function (providerName) {

    var provider = providersMapper[providerName]();

    firebase
    .app('facebook')
    .auth()
    .onAuthStateChanged(user => {
      console.log('[facebook] user: ', user);
    });

  firebase
    .app('twitter')
    .auth()
    .onAuthStateChanged(user => {
      console.log('[facebook] user: ', user);
    });

    return new Promise((accept, reject) => {

      firebase
      .app(providerName)
      .auth()
      .signInWithRedirect(provider)
      .then(() => {
        return firebase.app(providerName).auth().getRedirectResult();
      })
      .then(result => {
        $log.info('SignIn Success: logged user: ', this.loggedUser);
        accept(result.user);
      })
      .catch(err => {
        $log.error('SignIn Error: ', err);
        reject(err);
      });

    });
  }

  /**
   * Signs out the current user, by
   */
  this.signOut = function (providerName) {
    const selectedFirebase = firebase.app(providerName);
    const loggedOutUser = selectedFirebase.auth().currentUser;
    return new Promise((accept, reject) => {
      firebase.app(providerName).auth().signOut()
        .then(() => {          
          $log.info('SignOut Success: The user was successfully logged out.');
          accept(loggedOutUser);
        })
        .catch(err => {
          $log.error('SignOut Error: ', err);
          reject(err);
        });
    });

  }

  this.isSignedIn = function (providerName) {
    const selectedFirebase = firebase.app(providerName);
    return selectedFirebase.auth().currentUser != null;
  }

  this.getLoggedUser = function (providerName) {
    const selectedFirebase = firebase.app(providerName);
    return selectedFirebase.auth().currentUser;
  }
}