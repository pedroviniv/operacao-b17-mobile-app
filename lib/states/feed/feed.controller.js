export default function($state) {
  'ngInject';

  var vm = this;

  vm.posts = [
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      date: 'November 05, 1955',
      image: 'https://via.placeholder.com/256x150',
      data: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item, the content is from a card-body consisting of an image and paragraph text. The footer consists of tabs, icons aligned left, within the card-footer.',
      likes_count: 10,
      shares_count: 240
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      date: 'November 05, 1955',
      image: 'https://via.placeholder.com/256x150',
      data: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item, the content is from a card-body consisting of an image and paragraph text. The footer consists of tabs, icons aligned left, within the card-footer.',
      likes_count: 10,
      shares_count: 240
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      date: 'November 05, 1955',
      image: 'https://via.placeholder.com/256x150',
      data: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item, the content is from a card-body consisting of an image and paragraph text. The footer consists of tabs, icons aligned left, within the card-footer.',
      likes_count: 10,
      shares_count: 240
    }
  ];

  vm.newPost = function() {
    $state.go('newPost.gallery');
  };

  return vm;
}
