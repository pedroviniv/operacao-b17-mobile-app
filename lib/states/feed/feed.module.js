import controller from './feed.controller';
import config from './feed.config';
import post from '../../directives/feed/post/post.module';
import floatingButton from '../../directives/floating-button/floating-button.module';
import 'mfb/src/mfb.min.css';
import './style.css';
import 'mfb/src/mfb.min.js';
import 'ng-material-floating-button/src/mfb-directive';

const requires = [
  post,
  'ng-mfb',
  floatingButton
];

let feed = angular.module('app.feed', requires);
feed.config(config);
feed.controller('FeedController', controller);


export default feed = feed.name;