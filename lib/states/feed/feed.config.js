export default function($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider.state('home.feed', {
      url: '/feed',
      views: {
        'feed-tab': {
          template: require('./feed.html'),
          controller: 'FeedController as feedCtrl'
        }
      }
  });
}
