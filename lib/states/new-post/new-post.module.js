import config from './new-post.config';
import gallery from '../gallery/gallery.module';
import controller from './new-post.controller';

const requires = [
    'ionic',
    'ui.router',
    'app.constant',
    gallery
];

let newPost = angular.module('app.newPost', requires);
newPost.controller('NewPostController', controller);

newPost.config(config);

//exporting the name in order to use this export in the requires dependency array
export default newPost = newPost.name
