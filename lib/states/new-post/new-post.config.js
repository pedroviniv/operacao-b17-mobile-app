export default function($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider.state('newPost', {
      url: "/new-post",
      template: require("./new-post.html"),
      controller: 'NewPostController as newPostCtrl',
      abstract: true,
  });
}
