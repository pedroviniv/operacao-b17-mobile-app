export default function NewPostController($ionicHistory) {
  'ngInject';

  var vm = this;

  vm.goBack = function() {
    $ionicHistory.backView().go();
  }
}