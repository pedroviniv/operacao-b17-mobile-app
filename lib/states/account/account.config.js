export default function($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider.state('home.account', {
      url: '/account',
      views: {
        'account-tab': {
          template: require('./account.html'),
          controller: 'AccountController as accountCtrl'
        }
      }
  });
}
