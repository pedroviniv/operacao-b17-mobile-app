import controller from './account.controller';
import config from './account.config';
import activityItemList from '../../directives/activity/activity.module';

const requires = [
  activityItemList
];

let account = angular.module('app.account', requires);
account.config(config);
account.controller('AccountController', controller);


export default account = account.name;