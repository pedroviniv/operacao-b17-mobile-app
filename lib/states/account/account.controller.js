export default function($scope, $log) {
  'ngInject';

  var vm = this;

  console.log('account.controller');

  vm.activities = [
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    },
    {
      author: {
        picture: 'https://via.placeholder.com/32x32',
        name: 'Marty McFly'
      },
      location: 'Rio de Janeiro - PB'
    }
  ];

  vm.helloWorld = function() {
    alert('HI! :)');
  };

  return vm;
}
