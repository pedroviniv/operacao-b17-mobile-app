export default function($stateProvider, $urlRouterProvider) {
    'ngInject';
    $stateProvider.state('home', {
        url: "/home",
        template: require("./home.html"),
        abstract: true,
        redirectTo: 'home.feed'
    });
}
