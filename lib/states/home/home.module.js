import config from './home.config';
import controller from './home.controller';
import './style.css';
import feed from '../feed/feed.module';
import account from '../account/account.module';

const requires = [
    'ionic',
    'ui.router',
    'app.constant',
    feed,
    account
];

let home = angular.module('app.home', requires);

home.config(config);
home.controller('HomeController', controller);

//exporting the name in order to use this export in the requires dependency array
export default home = home.name
