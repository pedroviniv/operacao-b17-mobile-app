import config from './auth.config';
import controller from './auth.controller';
import authService from '../../services/auth/auth.module';
import twitterAuth from '../../directives/auth/twitter/twitter.module';
import facebookAuth from '../../directives/auth/facebook/facebook.module';
import './style.css';

const requires = [
  'ionic',
  'ui.router',
  'app.constant',
  authService,
  twitterAuth,
  facebookAuth
];

let auth = angular.module('app.auth', requires);

auth.config(config);
auth.controller('AuthController', controller);

export default auth = auth.name;