export default function($log, $state, authService) {
  'ngInject';

  var vm = this;

  vm.goHome = function() {
    if(authService.isSignedIn('twitter')) {
      $state.go('home.feed');
    } else {
      alert('Antes de prosseguir, você deve se autenticar com sua conta do Twitter.');
    }
  }
};