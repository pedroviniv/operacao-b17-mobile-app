export default ($stateProvider, $urlRouterProvider) => {
  'ngInject';

  $stateProvider
    .state('auth', {
      url: '/auth',
      template: require('./auth.html'),
      controller: 'AuthController as authCtrl',
      abstract: false
    });

  $urlRouterProvider.otherwise('/auth');
};