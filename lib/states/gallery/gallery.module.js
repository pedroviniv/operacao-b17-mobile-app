import controller from './gallery.controller';
import config from './gallery.config';

const requires = [

];

let gallery = angular.module('app.gallery', requires);
gallery.config(config);
gallery.controller('GalleryController', controller);


export default gallery = gallery.name;