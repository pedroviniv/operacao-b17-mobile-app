export default function($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider.state('newPost.gallery', {
      url: '/gallery',
      views: {
        'gallery-tab': {
          template: require('./gallery.html'),
          controller: 'GalleryController as galleryCtrl'
        }
      }
  });
}