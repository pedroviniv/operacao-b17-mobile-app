export default function ($rootScope, $state, $log, $ionicPlatform, firebaseConfig) {
  'ngInject';

  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  onStateChangeStart($rootScope, $state);

  $log.info("initializing firebase app");
  initFirebase($rootScope, firebase, firebaseConfig);

  $log.info('app running');
}

function initFirebase($rootScope, firebase, firebaseConfig) {
  firebase.initializeApp(firebaseConfig, 'twitter');
  firebase.initializeApp(firebaseConfig, 'facebook');

  firebase.app('facebook').auth().onAuthStateChanged((user) => {
    console.log('[run.js][facebook] onAuth: user ', user);
    if(user)
      $rootScope.$broadcast('facebook_user', user);
  });
  firebase.app('twitter').auth().onAuthStateChanged((user) => {
    console.log('[run.js][twitter] onAuth: user ', user);
    if(user)
      $rootScope.$broadcast('twitter_user', user);
  });
}

function onStateChangeStart($rootScope, $state) {

  $rootScope.$on('$stateChangeStart', function (evt, to, params) {
    console.log('to: ', to);
    if (to.redirectTo) {
      evt.preventDefault();
      $state.go(to.redirectTo, params, { location: 'replace' })
    }
  });
}

