const activityListItem = {
  name: 'activityListItem',
  options: function() {
    return {
      restrict: 'E',
      scope: {
        activity: '=activity'
      },
      template: require('./activity.html')
    };
  }
};

export default activityListItem;