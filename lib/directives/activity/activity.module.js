import directive from './activity.directive';

const requires = [

];

let activity = angular.module('app.activity', requires);
activity.directive(directive.name, directive.options);

export default activity = activity.name;