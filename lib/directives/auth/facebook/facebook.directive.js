import controller from './facebook.controller';

const facebookAuthButton = {
  name: 'facebookAuthButton',
  options: function() {
    return {
      restrict: 'E',
      controller: controller,
      controllerAs: 'facebookAuthCtrl',
      template: require('./facebook.html'),
      bindToController: true
    };
  }
}

export default facebookAuthButton;