//using firebase service to authenticate
export default function FacebookFirebaseAuthCtrl($scope, $state, $log, authService) {
  'ngInject';

  var vm = this;
  
  vm.toggle = false;

  $scope.$on('facebook_user', (e, user) => {
    $scope.$apply(() => {
      vm.toggle = true;
    });
  });

  function authenticate() {

    if(vm.toggle) {

      console.log('authentication via facebook');
      authService.signIn('facebook')
        .then(loggedUser => console.log('loggedUser: ', loggedUser))
        .catch(console.log);

    } else {
      authService.signOut('facebook')
        .then(loggedOut => console.log(loggedOut))
        .catch(err => console.log(err));
    }
  }

  vm.authenticate = authenticate;
};