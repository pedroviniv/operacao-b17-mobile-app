import facebookAuthButton from './facebook.directive';
import authService from '../../../services/auth/auth.module';

const requires = [
  authService
];

let facebookAuth = angular
  .module('app.auth.facebook', requires)
  .directive(facebookAuthButton.name, facebookAuthButton.options);

export default facebookAuth = facebookAuth.name;