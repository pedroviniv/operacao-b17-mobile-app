//using firebase service to authenticate
export default function TwitterFirebaseAuthCtrl($scope, $log, authService) {
  'ngInject';

  var vm = this;
  vm.toggle = false;
  
  $scope.$on('twitter_user', (e, user) => {
    $scope.$apply(() => {
      vm.toggle = true;
    });
  });

  console.log('authService: ', authService);

  function authenticate() {

    if(vm.toggle) {

    console.log('authentication via twitter');
    authService.signIn('twitter')
      .then(loggedUser => console.log('loggedUser: ', loggedUser))
      .catch(console.log);

    } else {
      authService.signOut('twitter')
        .then(loggedOut => console.log(loggedOut))
        .catch(err => console.log(err));
    }
  }

  vm.authenticate = authenticate;
};