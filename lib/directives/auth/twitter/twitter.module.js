import twitterAuthButton from './twitter.directive';
import authService from '../../../services/auth/auth.module';

const requires = [
  authService
];

let twitterAuth = angular
  .module('app.auth.twitter', requires)
  .directive(twitterAuthButton.name, twitterAuthButton.options);

export default twitterAuth = twitterAuth.name;