import controller from './twitter.controller';


const twitterAuthButton = {
  name: 'twitterAuthButton',
  options: function() {
    return {
      restrict: 'E',
      controller: controller,
      controllerAs: 'twitterAuthCtrl',
      template: require('./twitter.html'),
      bindToController: true
    };
  }
}

export default twitterAuthButton;