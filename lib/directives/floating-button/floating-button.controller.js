export default function FloatingButtonController($scope) {
  'ngInject';

  var vm = this;

  vm.executeCallback = function() {
    if($scope.onTapCallback) {
      $scope.onTapCallback();
    }
  }
}