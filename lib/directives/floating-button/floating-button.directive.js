import controller from './floating-button.controller';

const floatingButton = {
  name: 'floatingButton',
  options: function() {
    return {
      restrict: 'E',
      scope: {
        buttonClass: '@buttonClass',
        restingButtonIcon: '@restingButtonIcon',
        activeButtonIcon: '@activeButtonIcon',
        onTapCallback: '&onTapCallback'
      },
      template: require('./floating-button.html'),
      controller: controller,
      controllerAs: 'floatingButtonCtrl'
    };
  }
};

export default floatingButton;