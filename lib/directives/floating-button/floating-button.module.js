import directive from './floating-button.directive';
import 'mfb/src/mfb.css';
import 'mfb/src/mfb.js';
import 'ng-material-floating-button/src/mfb-directive';


const requires = [
  'ng-mfb'
];

let floatingButton = angular.module('app.floating-button', requires);
floatingButton.directive(directive.name, directive.options);

export default floatingButton = floatingButton.name;