import directive from './post.directive';

const requires = [

];

let post = angular.module('app.feed.post', requires);
post.directive(directive.name, directive.options);

export default post = post.name;