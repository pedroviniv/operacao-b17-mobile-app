const postCard = {
  name: 'postCard',
  options: function() {
    return {
      restrict: 'E',
      scope: {
        post: '=post'
      },
      template: require('./post.html')
    };
  }
};

export default postCard;